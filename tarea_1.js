/*
2) El conjunto de datos contiene 4 calificaciones de n estudiantes. Confirma que se importo correctamente la colección con los siguientes comandos en la terminal de mongo: >use students; >db.grades.count() ¿Cuántos registros arrojo el comando count?
db.grades.count();
800

3) Encuentra todas las calificaciones del estudiante con el id numero 4.
db.grades.find({ "student_id": 4 });
{ "_id" : ObjectId("50906d7fa3c412bb040eb589"), "student_id" : 4, "type" : "homework", "score" : 5.244452510818443 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb588"), "student_id" : 4, "type" : "quiz", "score" : 27.29006335059361 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb58a"), "student_id" : 4, "type" : "homework", "score" : 28.656451042441 }
{ "_id" : ObjectId("50906d7fa3c412bb040eb587"), "student_id" : 4, "type" : "exam", "score" : 87.89071881934647 }


4) ¿Cuántos registros hay de tipo exam?
db.grades.find({ "type": "exam" }).count();
200

5) ¿Cuántos registros hay de tipo homework?
db.grades.find({ "type": "homework" }).count();
400

6) ¿Cuántos registros hay de tipo quiz?
db.grades.find({ "type": "quiz" }).count();
200

7) Elimina todas las calificaciones del estudiante con el id numero 3
db.grades.remove({ "student_id": 3 });
WriteResult({ "nRemoved" : 4 })


8) ¿Qué estudiantes obtuvieron 75.29561445722392 en una tarea ?
db.grades.find({ "score": 75.29561445722392 });
{ "_id" : ObjectId("50906d7fa3c412bb040eb59e"), "student_id" : 9, "type" : "homework", "score" : 75.29561445722392 }

9) Actualiza las calificaciones del registro con el uuid 50906d7fa3c412bb040eb591 por 100
db.grades.update({ "_id": ObjectId("50906d7fa3c412bb040eb591") }, { $set: { "score": 100 } });
WriteResult({ "nMatched" : 1, "nUpserted" : 0, "nModified" : 1 })

10) A qué estudiante pertenece esta calificación.
db.grades.find({ "_id": ObjectId("50906d7fa3c412bb040eb591") });
{ "_id" : ObjectId("50906d7fa3c412bb040eb591"), "student_id" : 6, "type" : "homework", "score" : 100 }




















//respuesta de terminal:
//axelmoncloamuro@MacBook-Air-de-Axel tarea1 % mongo

//MongoDB shell version v5.0.21
//connecting to: mongodb://127.0.0.1:27017/?compressors=disabled&gssapiServiceName=mongodb
//Implicit session: session { "id" : UUID("eb14f105-352f-467e-9b6b-383ebf06746b") }
//MongoDB server version: 7.0.2
//WARNING: shell and server versions do not match
//================
//Warning: the "mongo" shell has been superseded by "mongosh",
//which delivers improved usability and compatibility.The "mongo" shell has been deprecated and will be removed in
//an upcoming release.
//For installation instructions, see
//https://docs.mongodb.com/mongodb-shell/install/
//================
//Welcome to the MongoDB shell.
//For interactive help, type "help".
//For more comprehensive documentation, see
//	https://docs.mongodb.com/
//Questions? Try the MongoDB Developer Community Forums
//	https://community.mongodb.com
//---
//The server generated these startup warnings when booting: 
//        2023-10-11T09:23:35.242-06:00: Access control is not enabled for the database. Read and write access to data and configuration is unrestricted
//       2023-10-11T09:23:35.242-06:00: This server is bound to localhost. Remote systems will be unable to connect to this server. Start the server with --bind_ip <address> to specify which IP addresses it should serve responses from, or with --bind_ip_all to bind to all interfaces. If this behavior is desired, start the server with --bind_ip 127.0.0.1 to disable this warning
//        2023-10-11T09:23:35.242-06:00: Soft rlimits for open file descriptors too low
//        2023-10-11T09:23:35.242-06:00:         currentValue: 256
//        2023-10-11T09:23:35.242-06:00:         recommendedMinimum: 64000
//---
//> use students;
//switched to db students
//> db.grades.count();
//800
//> db.grades.find({ "student_id": 4 });
//{ "_id" : ObjectId("50906d7fa3c412bb040eb589"), "student_id" : 4, "type" : "homework", "score" : 5.244452510818443 }
//{ "_id" : ObjectId("50906d7fa3c412bb040eb588"), "student_id" : 4, "type" : "quiz", "score" : 27.29006335059361 }
//{ "_id" : ObjectId("50906d7fa3c412bb040eb58a"), "student_id" : 4, "type" : "homework", "score" : 28.656451042441 }
//{ "_id" : ObjectId("50906d7fa3c412bb040eb587"), "student_id" : 4, "type" : "exam", "score" : 87.89071881934647 }
//> db.grades.find({ "type": "exam" }).count();
//200
//> db.grades.find({ "type": "homework" }).count();
//400
//> db.grades.find({ "type": "quiz" }).count();
//200
//> db.grades.remove({ "student_id": 3 });
//WriteResult({ "nRemoved" : 4 })
//> db.grades.find({ "score": 75.29561445722392 });
//{ "_id" : ObjectId("50906d7fa3c412bb040eb59e"), "student_id" : 9, "type" : "homework", "score" : 75.29561445722392 }
//> db.grades.update({ "_id": ObjectId("50906d7fa3c412bb040eb591") }, { $set: { "score": 100 } });
//WriteResult({ "nMatched" : 1, "nUpserted" : 0, "nModified" : 1 })
//> db.grades.find({ "_id": ObjectId("50906d7fa3c412bb040eb591") });
//{ "_id" : ObjectId("50906d7fa3c412bb040eb591"), "student_id" : 6, "type" : "homework", "score" : 100 }
//> ^C
//bye
/*


*/